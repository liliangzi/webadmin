package com.llz.webadmin.controller;

import com.llz.webadmin.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.List;

@Controller
public class TableController {

    @GetMapping("/basic_table")
    public String basicTable() {

        return "table/basic_table";
    }

    @GetMapping("/dynamic_table")
    public String dynamicTable(Model model) {
        List<User> userList = Arrays.asList(new User("name1", "password1"),
                new User("name2", "password2"),
                new User("name3", "password3"),
                new User("name4", "password4"),
                new User("name5", "password5"));
        model.addAttribute(userList);
        return "table/dynamic_table";
    }

    @GetMapping("/editable_table")
    public String editableTable() {

        return "table/editable_table";
    }

    @GetMapping("/pricing_table")
    public String pricingTable() {

        return "table/pricing_table";
    }

    @GetMapping("/responsive_table")
    public String responsiveTable() {

        return "table/responsive_table";
    }
}
