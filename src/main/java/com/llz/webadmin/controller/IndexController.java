package com.llz.webadmin.controller;

import com.llz.webadmin.bean.User;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {

    @GetMapping(value = {"/login", "/"})
    public String login() {
        return "login";
    }

    @PostMapping("/login")
    public String index(User user, HttpSession session, Model model) {

        if (StringUtils.hasLength(user.getUserName()) && "123456".equals(user.getPassword())) {
            session.setAttribute("loginUser", user);
            return "redirect:/index.html";
        } else {
            model.addAttribute("msg", "账号密码错误！");
            return "login";
        }
    }

    @GetMapping("/index.html")
    public String indexPage(HttpSession session, Model model) {
        if (null != session.getAttribute("loginUser")) {
            return "index";
        } else {
            model.addAttribute("msg", "请登录！");
            return "login";
        }
    }


}
